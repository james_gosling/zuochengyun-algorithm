public class HeapSort {
    public static void main(String[] args) {
        int[] int1 = AlgoUtils.generateRandomArray(20, 20);
        int[] int2 = AlgoUtils.copyArray(int1);

        AlgoUtils.comparator(int1);
        heapSort(int2);

        AlgoUtils.printArray(int1);
        AlgoUtils.printArray(int2);

        System.out.println(AlgoUtils.isEqual(int1, int2));

    }

    public static void heapSort(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
        for (int i = 0; i < arr.length; i++) {
            heapInsert(arr, i); // O(logN)
        }
        int heapSize = arr.length;
        AlgoUtils.swap(arr, 0, --heapSize);
        while (heapSize > 0) { // O(logN)
            heapify(arr, 0, heapSize); // O(logN)
            AlgoUtils.swap(arr, 0, --heapSize); // O(1)
        }
    }

    // 某个数在i位置，能否往下移动
    private static void heapify(int[] arr, int i, int heapSize) {
        int left = (i << 1) + 1; // 左子的下标
        while (left < heapSize) { // 下方还有子时
            // 两子中，谁的值大，下标给largest
            int largest = left + 1 < heapSize && arr[left + 1] > arr[left]
                    ? left + 1 : left;
            // 父与较大子之间，谁的值大，下标给largest
            largest = arr[largest] > arr[i] ? largest : i;
            if (largest == i) {
                break;
            }
            AlgoUtils.swap(arr, largest, i);
            i = largest;
            left = (i << 1) + 1;
        }
    }

    // 某个数在i位置，向顶部移动
    private static void heapInsert(int[] arr, int i) {
        while (arr[i] > arr[(i - 1) / 2]) {
            AlgoUtils.swap(arr, i, (i - 1) / 2);
            i = (i - 1) / 2;
        }
    }
}

